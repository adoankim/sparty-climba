﻿using UnityEngine;
using System.Collections;

public class CannonActivator : MonoBehaviour {

	public TimedCannonFire timedCannonFire;

	void OnTriggerEnter2D(Collider2D collision)
	{
		if ((collision.tag == "Player"))
		{
			timedCannonFire.canFire = true;
		}
	}

	void OnTriggerExit2D(Collider2D collision)
	{
		if ((collision.tag == "Player"))
		{
			timedCannonFire.canFire = false;
		}
	}
}
