﻿using UnityEngine;
using System.Collections;

public class CannonBall : MonoBehaviour {

	public float moveSpeed = 4f;  // enemy move speed when moving
	public int damageAmount = 10; // probably deal a lot of damage to kill player immediately
	public float lifeTime = 3f;
	public bool faceLeft = true;
	public AudioClip audioClip;

	Transform _transform;
	SpriteRenderer _spriteRenderer;
	Rigidbody2D _rigidbody;
	ParticleSystem _particleSystem;
	AudioSource _audio;
	float _lifeTime;
	bool _collision;
	bool _dead;
	int _faceLeftFlag = -1;
	public void setFaceLeft(bool faceLeft){
		if (faceLeft) {
			_faceLeftFlag = -1;
		} else {
			_faceLeftFlag = 1;
		}
	}
	void Awake(){
		// get a reference to the components we are going to be changing and store a reference for efficiency purposes
		_transform = GetComponent<Transform> ();

		_rigidbody = GetComponent<Rigidbody2D> ();
		if (_rigidbody==null) // if Rigidbody is missing
			Debug.LogError("Rigidbody2D component missing from this gameobject");

		_particleSystem = GetComponent<ParticleSystem> ();
		if (_particleSystem==null) // if Rigidbody is missing
			Debug.LogError("ParticleSystem component missing from this gameobject");

		_spriteRenderer = GetComponent<SpriteRenderer> ();
		if (_spriteRenderer==null) // if Rigidbody is missing
			Debug.LogError("SpriteRenderer component missing from this gameobject");


		_audio = GetComponent<AudioSource> ();
		if (_audio==null) { // if AudioSource is missing
			Debug.LogWarning("AudioSource component missing from this gameobject. Adding one.");
			// let's just add the AudioSource component dynamically
			_audio = gameObject.AddComponent<AudioSource>();
		}

		_lifeTime = Time.time + lifeTime;
		_collision = false;
		_dead = false;
	}

	void Update () {
		if (!_collision) {
			
			if (Time.time < _lifeTime) {
				_rigidbody.velocity = new Vector2 (_faceLeftFlag * _transform.localScale.x * moveSpeed, _rigidbody.velocity.y);
			} else if(!_dead) {
				_dead = true;
				_rigidbody.velocity = new Vector2 (0, 0);
				explode ();

				StartCoroutine (destroyAfterNSeconds(0.25f));
			}

		}
	}

	void OnTriggerEnter2D(Collider2D collision)
	{
		if ((collision.tag == "Player"))
		{
			CharacterController2D player = collision.gameObject.GetComponent<CharacterController2D>();
			if (player.playerCanMove) {
				_collision = true;
				_lifeTime = 0;
				explode ();
				player.ApplyDamage (damageAmount);
				StartCoroutine (destroyAfterNSeconds(0.25f));
			}
		}
	}

	void explode ()
	{
		_spriteRenderer.enabled = false;
		_audio.PlayOneShot (audioClip);
		_particleSystem.Play ();
	}

	IEnumerator destroyAfterNSeconds (float seconds)
	{
		yield return new WaitForSeconds (seconds);
		DestroyObject(gameObject);
	}
}
