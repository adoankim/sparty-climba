﻿using UnityEngine;
using System.Collections;

public class CannonBallSpawner : MonoBehaviour {
	public GameObject cannonBallPrefab;

	public void spawn (bool facedLeft)
	{
		GameObject cannonBall = (GameObject) Instantiate (cannonBallPrefab, transform.position, transform.rotation);
		CannonBall cannonBallScript = cannonBall.GetComponent<CannonBall> ();
		cannonBallScript.setFaceLeft (facedLeft);
		cannonBallScript.enabled = true;

	}
}
