﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class FadeAndLoad : Task {

	public string levelName;

	public override void run(){
		StartCoroutine (DoFadeAndLoad());
	}

	IEnumerator DoFadeAndLoad(){
		float fadeTime = gameObject.GetComponent<Fading> ().BeginFade (1);
		yield return new WaitForSeconds (fadeTime);
		SceneManager.LoadScene (levelName);
	}
}
