﻿using UnityEngine;
using System.Collections;

public class GameOverScript : MonoBehaviour {
	public GameObject player;
	public AudioSource mainTheme;

	// Use this for initialization
	void Awake () {
		Die ();
	}


	public void Die(){
		player.SetActive (false);
		mainTheme.Stop ();
		GetComponent<AudioSource> ().Play ();
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space)) {
			GameManager.gm.RestartLevel();
			mainTheme.Play ();
		}
	}
}
