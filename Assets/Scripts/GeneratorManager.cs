﻿using UnityEngine;
using System.Collections;

public class GeneratorManager : MonoBehaviour {
	
	public LevelGeneration[] levelGeneration;
	public int[] startPosition;

	public int currentLevel;
	private int _swapLevelPos = 0;
	private int _startPoint = 201;
	private int _increment = 72;
	private int _meters = 300;
	void Start () {
		currentLevel = -1;
	}
	public void Reset(){
		int n = 0;
		int meters = 100;
		_startPoint = 201;
		_increment = 72;
		_meters = 300;
		_swapLevelPos = 0;
		currentLevel = -1;
		foreach(LevelGeneration lg in levelGeneration){
			Vector3 position = lg.transform.localPosition;
			position.y = startPosition[n++];
			lg.transform.localPosition = position;
			GameManager.gm.level = n;
			lg.GenerateLevel ();
			lg.transform
				.FindChild ("Indicator")
				.GetComponent<TextMesh> ()
				.text = meters + " m";
			lg.transform.FindChild ("LowerLimit").gameObject.SetActive (false);
			lg.transform.FindChild ("UpperLimit").gameObject.SetActive (true);
			meters = meters + 100;
		}	
		GameManager.gm.level = 4;
	
	}
	public void SwapLevel(){
			levelGeneration [_swapLevelPos]
			.transform
			.FindChild ("LowerLimit")
			.gameObject.SetActive(false);


		levelGeneration [_swapLevelPos]
			.transform
			.FindChild ("UpperLimit")
			.gameObject.SetActive(true);

		_meters = _meters + 100;
		levelGeneration [_swapLevelPos]
			.transform
			.FindChild ("Indicator")
			.GetComponent<TextMesh> ()
			.text = _meters + " m";
		Debug.Log ("swap!");
		levelGeneration [_swapLevelPos].GenerateLevel ();
		Vector3 position = levelGeneration [_swapLevelPos]
			.transform.localPosition;
		_startPoint = _startPoint + _increment;
		position.y = _startPoint;
		levelGeneration [_swapLevelPos].transform.localPosition = position;
		_swapLevelPos = (_swapLevelPos + 1) % (levelGeneration.Length);
		Debug.Log ("swap " + _swapLevelPos);

		currentLevel = 0;
	}
}
