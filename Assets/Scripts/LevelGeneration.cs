﻿using UnityEngine;
using System.Collections;

public class LevelGeneration : MonoBehaviour {
	public GameObject[] levelPartPlaces;
	public GameObject[] level1Prefabs;
	public GameObject[] level2Prefabs;
	public GameObject[] level3Prefabs;
	public GameObject[] level4Prefabs;
	public int startLevel = 1;
	private GameObject[] _levelPrefabs;
	// Use this for initialization
	void Start () {
		
		_GenGen (startLevel);
	}
	
	public void GenerateLevel(){
		_GenGen (GameManager.gm.level);
	}
	private void _GenGen(int lvl){
		if (lvl == 2) {
			_Generate (level2Prefabs);
		} else if (lvl == 3) {
			_Generate (level3Prefabs);
		} else if (lvl == 4) {
			_Generate (level4Prefabs);
		} else {
			_Generate (level1Prefabs);
		}

	}
	private void _Generate(GameObject[] levelPrefabs){

		int levelPrefabsLength = levelPrefabs.Length - 1;
		GameObject levelPart;
		foreach(GameObject levelPartPlace in levelPartPlaces){
			GenPack gp = levelPartPlace.GetComponentInChildren<GenPack> ();

			if (gp != null) {
				Destroy (gp.gameObject);
			}
			try{
				levelPart = (GameObject)Instantiate(levelPrefabs[Random.Range (0, levelPrefabsLength)]);
				levelPart.transform.SetParent (levelPartPlace.transform, false);
			}catch(System.Exception e){
				Debug.LogError (e.Message);
			}

		}
	}

}