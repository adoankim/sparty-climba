﻿using UnityEngine;
using System.Collections;

public class LevelLimitCollider : MonoBehaviour {
	public string limitType;
	public GeneratorManager generatorManager;
	private bool _isUpperBound;
	private AudioSource _audioSource;
	void Awake(){
		_isUpperBound = limitType == "upper";
		_audioSource = GetComponent<AudioSource> ();
	}

	void OnTriggerEnter2D(Collider2D collision)
	{
		if ((collision.tag == "Player"))
		{
			gameObject.SetActive (false);
			if (_isUpperBound) {
				generatorManager.currentLevel++;
				collision.gameObject.GetComponent<CharacterController2D> ().CollectCoin (100);
				_audioSource.Play();
			} else {
				collision.gameObject.GetComponent<CharacterController2D>().FallDeath ();
			}

			if (generatorManager.currentLevel == 1) {
				GameObject lowerLimit =  gameObject.transform.parent.Find("LowerLimit").gameObject;
				lowerLimit.SetActive (true);
				
				generatorManager.SwapLevel ();
			}
		}
	}
}
