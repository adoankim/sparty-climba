﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class OnKeyLoadScene : MonoBehaviour {
	public string sceneName;

	void Update () {
		if (Input.GetKeyDown (KeyCode.Space)) {
			SceneManager.LoadScene (sceneName);
		}	
	}
}
