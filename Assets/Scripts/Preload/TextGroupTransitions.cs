﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class TextGroupTransitions : MonoBehaviour {

	public string endTransitionBoolName = "";
	public Text[] textGroup;
	public float[] transitionSeconds;
	public Task task;
	private int position;

	void Start () {
		position = 0;
		StartCoroutine (TextTransite ());
	}

	IEnumerator TextTransite(){

		if (!textGroup [position].IsActive ()) {
			textGroup [position].gameObject.SetActive (true);
			yield return new WaitForSeconds (1);
		}

		yield return new WaitForSeconds (transitionSeconds[position]);

		textGroup [position].GetComponent<Animator> ().SetBool (endTransitionBoolName, true);

		yield return new WaitForSeconds (1);

		textGroup [position].gameObject.SetActive (false);
		position++;

		if (position < textGroup.Length) {
			StartCoroutine (TextTransite ());
		} else if(task != null) {
			task.run ();
		}
	}

}
