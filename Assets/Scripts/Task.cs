﻿using UnityEngine;

public abstract class Task : MonoBehaviour {
	public abstract void run();
}
