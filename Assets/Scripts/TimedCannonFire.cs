﻿using UnityEngine;
using System.Collections;

public class TimedCannonFire : MonoBehaviour {

	[HideInInspector]
	public bool canFire = false;

	public CannonBallSpawner cannonBallSpawner;


	public GameObject fireParticleEmitter;
	public float timeRepetition = 3f;
	// Use this for initialization
	private float _fireTime;
	private Animator _animator;
	private bool _firing;
	private bool _facedLeft = true;
	void Start () {
		_animator = GetComponent<Animator> ();
		_fireTime = Time.time + timeRepetition;
		_firing = false;
		_facedLeft = transform.localScale.x > 0;
	}

	void Update () {

		if (canFire && !_firing && Time.time > _fireTime) {
			_firing = true;
			StartCoroutine (fireCannonBall ());
		}

	}
		
	IEnumerator fireCannonBall ()
	{
		_animator.SetBool ("fire", true);
		yield return new WaitForSeconds (0.5f);
		cannonBallSpawner.spawn(_facedLeft);
		yield return new WaitForSeconds (0.3f);
		_animator.SetBool ("fire", false);
		_fireTime = Time.time + timeRepetition;
		_firing = false;
	}
}
